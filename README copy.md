This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
pnpm dev
```

# tech stack:


# ORM - Prisma:
```
npx prisma init


npx prisma migrate dev
npx prisma studio

```

https://www.prisma.io/docs/getting-started/setup-prisma/start-from-scratch/relational-databases/install-prisma-client-typescript-postgresql

### Run / Create - Prisma schema
```
npm run prisma:init  
```


# Animation :
https://lottiefiles.com/

https://lottiefiles.com/animations/graph-animation-msUHUTejxN


# Auth0
npm install @auth0/auth0-react

https://github.com/auth0/nextjs-auth0?tab=readme-ov-file#app-router

https://developer.auth0.com/resources/guides/web-app/nextjs/basic-authentication

[doc Nextjs-auth0](https://auth0.github.io/nextjs-auth0/types/helpers_with_page_auth_required.WithPageAuthRequiredAppRouter.html)

https://github.com/auth0/nextjs-auth0/blob/main/EXAMPLES.md
https://auth0.com/blog/ultimate-guide-nextjs-authentication-auth0/

[Next.js/JavaScript App Router Code Sample](https://developer.auth0.com/resources/code-samples/web-app/nextjs/basic-authentication?_gl=1*o4jjf3*_gcl_aw*R0NMLjE3MDc3MTI3OTYuQ2p3S0NBaUFfYUd1QmhBQ0Vpd0FseTU3TWN1TUpFVExsOXg0d1JRUUFNNXNleEF4dk1vdnhyeGduekJnbEc5MEc5eEV5S0U0NGNkUk5Cb0NyM0FRQXZEX0J3RQ..*_gcl_au*NTM2MDc5NTg0LjE3MDc2OTkwMzY.*_ga*MjkzMTk4MTY5LjE3MDc2OTkwMzc.*_ga_QKMSDV5369*MTcwNzcxMjc5Ni4zLjEuMTcwNzcxMzE4OS41OS4wLjA.)

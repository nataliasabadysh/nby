"use client";
import { useUser } from "@auth0/nextjs-auth0/client";
// export { UserProvider, UserProviderProps, UserProfile, UserContext, RequestError, useUser };
// export { withPageAuthRequired, WithPageAuthRequired, WithPageAuthRequiredOptions };

export default function User() {
  const { user, error, isLoading } = useUser();

  console.log("error", error);
  console.log("user", user);
  console.log("isLoading", isLoading);

  // if (isLoading) return <div>Loading...</div>;
  // if (error) return <div>{error.message}</div>;

  // if (user) {
  //   return (
  //     <div>
  //       Welcome {user.name}! <a href="/api/auth/logout">Logout</a>
  //     </div>
  //   );
  // }

  return (
    <a
      href="/api/auth/login"
      style={{ fontSize: 50, color: "red", display: "block" }}
    >
      Login
    </a>
  );
}

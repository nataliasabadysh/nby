import { Metadata } from "next";
import Image from "next/image";

import styles from "./page.module.css";
import { Header } from "./componnents/header/index";
import { Hero } from "./componnents/header/hero";
import { Title } from "./componnents/title";
import { Footer } from "./componnents/footer/index";
import User from './auth_user';

const path = "/bg.jpg";


export default  function Home() {

  return (
    <>
      <main>  
      <a href="/api/auth/login">Login</a>
         {/* <User /> */}
        <Hero />
     
        <section className="container">
          <div className={styles.container_about}>
            <Title
              type="h2"
              className="sub_title"
              title="our mission it is to take the AI and 
              other possibility to benefit every single person in your team"
            />

            <div
              className={`${styles.mission} ${styles.list_about_us}`}
              id="about"
            >
              <div>
                <h3> Our Mission</h3>
                <p>
                  {" "}
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Est
                  fugiat libero deserunt ipsam magnam aut repellat quibusdam
                  doloribus, nulla.
                </p>
              </div>
              <div>
                <h3> Our Mission</h3>
                <p>
                  {" "}
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Est
                  fugiat libero deserunt ipsam magnam aut repellat quibusdam
                  doloribus, nulla.
                </p>
              </div>
              <div>
                <h3> Our Mission</h3>
                <p>
                  {" "}
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Est
                  fugiat libero deserunt ipsam magnam aut repellat quibusdam
                  doloribus, nulla.
                </p>
              </div>

              <div>
                <h3> Our Mission</h3>
                <p>
                  {" "}
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Est
                  fugiat libero deserunt ipsam magnam aut repellat quibusdam
                  doloribus, nulla.
                </p>
              </div>
              <div>
                <h3> Our Mission</h3>
                <p>
                  {" "}
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Est
                  fugiat libero deserunt ipsam magnam aut repellat quibusdam
                  doloribus, nulla.
                </p>
              </div>
              <div>
                <h3> Our Mission</h3>
                <p>
                  {" "}
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Est
                  fugiat libero deserunt ipsam magnam aut repellat quibusdam
                  doloribus, nulla.
                </p>
              </div>
            </div>
          </div>
        </section>
      </main>

      <Footer />
    </>
  );
}

import type { Metadata } from "next";
import styles from "./page.module.css";
import { Sidebar } from "../componnents/sidebar";

export const metadata: Metadata = {
  title: "Nobody But You",
  description: "Career road map for ",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body>
        <div className={styles.wrapper}>
          <div className={styles.container}>
            <div>{children}</div>
          </div>

          <aside className={styles.aside}>
            <Sidebar />
          </aside>
        </div>
      </body>
    </html>
  );
}

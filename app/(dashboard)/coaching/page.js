"use client";
import Link from "next/link";
import styles from "./styles.module.css";

const title = "What is your dream destination Today?";

const options = [
  {
    title: "grow",
    desc: "lorem lorem"
  },
  {
    title: "score",
    desc: "lorem lorem"
  },
  {
    title: "smartef",
    desc: "lorem lorem"
  },
  {
    title: "ikigai",
    desc: "lorem lorem"
  }
];

export default function CoachingSupport() {
  return (
    <div className={styles.container}>
      {options.map(({ title, desc }) => (
        <Link href={`/coaching/${title}`} className={styles.item} key={title}>
          <h1>{title}</h1>
          <p>{desc}</p>
        </Link>
      ))}
    </div>
  );
}

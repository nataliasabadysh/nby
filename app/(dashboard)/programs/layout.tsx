import type { Metadata } from "next";

export const metadata: Metadata = {
  title: "About Page",
  description: "About page descriptions",
};

export default async function AdminLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  // const user = await getUser();

  return <div>{children}</div>;
}

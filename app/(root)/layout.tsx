import type { Metadata } from "next";

export const metadata: Metadata = {
  title: "Nobody But You",
  description: "Career road map for ",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  );
}

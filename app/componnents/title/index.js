import { createElement } from "react";

export const Title = ({
  title = "Default title",
  className = "title",
  type = "h1",
}) => {
  return createElement(type, { className: className }, title);
};

"use client";
import Link from "next/link";
import styles from "./styles.module.css";
import { usePathname } from "next/navigation";

export const user_book = [
  {
    href: "/calendar",
    label: "calendar",
  },
  {
    href: "/dashboard",
    label: "dashboard",
  },
  {
    href: "/programs",
    label: "programs",
  },
  {
    href: "/chat",
    label: "chat AI",
  },
  {
    href: "/coaching",
    label: "coaching game",
  },
];

const NavLink = ({ href, label }) => {
  const pathname = usePathname();

  const isActive = href.startsWith(pathname);
  return (
    <Link href={href} className={isActive ? styles.active_link : null}>
      {label}
    </Link>
  );
};
export const Sidebar = () => {
  return (
    <ul className={styles.list}>
      {user_book.map((link) => {
        return (
          <li key={link.href}>
            <NavLink href={link.href} label={link.label} />
          </li>
        );
      })}
    </ul>
  );
};

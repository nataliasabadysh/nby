import Link from "next/link";
import styles from "./styles.module.css";

export const Footer = () => {
  return (
    <footer className={styles.footer}>
      <div className="container">
        <p>Footer for this website</p>
      </div>
    </footer>
  );
};

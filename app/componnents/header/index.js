"use client";

import { usePathname } from "next/navigation";
import Link from "next/link";
import styles from "./styles.module.css";
// import { Link as ScrollLink } from "react-scroll";

export const Header = () => {
  const pathname = usePathname();

  const root = ["/", "./about", "/blog"].includes(pathname);

  return (
    <header className={styles.header}>
      <div className="container">
        <nav className={styles.nav}>
          <Link href="/">
            <p className={styles.navLink}>
              <b>nobody but you</b>
            </p>
          </Link>

          {root && (
            <ul className={styles.navList}>
              <li className={styles.navItem}>
                <Link href="/">
                  <p
                    className={`${styles.navLink} ${
                      pathname === "/" ? "active" : ""
                    }`}
                  >
                    home
                  </p>
                </Link>
                {/* <Link to="about" smooth={true} duration={500}>About</Link> */}
              </li>
              <li className={styles.navItem}>
                <Link href="/blog">
                  <p
                    className={`${styles.navLink} ${
                      pathname === "/blog/" ? "active" : ""
                    }`}
                  >
                    blog
                  </p>
                </Link>
              </li>
              <li className={styles.navItem}>
                <Link href="/about">
                  <p
                    className={`${styles.navLink} ${
                      pathname === "/about/" ? "active" : ""
                    }`}
                  >
                    about
                  </p>
                </Link>
              </li>
            </ul>
          )}
          {root ? (
            <button className={styles.btn}>join</button>
          ) : (
            <button className={styles.btn}>log out</button>
          )}
        </nav>
      </div>
    </header>
  );
};

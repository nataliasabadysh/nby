import Image from "next/image";

import styles from "./styles.module.css";

const path = "/bg.jpg";

export const Hero = () => {
  return (
    <>
      <div className={styles.hero}>
        <div className={`${styles.wrapper} container `}>
          <h1 className="title">Influence Employee</h1>
          <h1 className="title">Visualise the Goal </h1>
          <h1 className="title">Visualise the Goal </h1>
          <iframe src="https://lottie.host/embed/d376221b-d702-443f-8d1c-845ed109fb19/pVxp2Dr9o3.json"></iframe>
        </div>
      </div>

      <div className={`${styles.wrapper_img}`}>
        <Image
          src={path}
          width={1840}
          height={400}
          sizes="100wv"
          alt="BG image"
          className={styles.hero_img}
          priority
        />

        <div className={`${styles.mission} container`}>
          <div className={styles.mission_block}>
            <p>
              Цю підбірку ми створили спеціально для дизайнерів, щоб вам було
              легше
            </p>
          </div>

          <div className={styles.block}>
            <span>icon</span>
            <p>block 3</p>
          </div>

          <div className={styles.block}>
            <span>icon</span>
            <p>block 3</p>
          </div>

          <div className={styles.block}>
            <span>icon</span>
            <p>block 3</p>
          </div>
          <div></div>
          <div className={styles.block}>
            <span>icon</span>
            <p>block 3</p>
          </div>
          <div className={styles.block}>
            <span>icon</span>
            <p>block 3</p>
          </div>
          <div className={styles.block}>
            <span>icon</span>
            <p>block 3</p>
          </div>
        </div>
      </div>
    </>
  );
};
